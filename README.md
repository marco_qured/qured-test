## Qured Test!

The main goal of the test is to show a list of *posts*, preferably in a *RecyclerView* without handling any action on the items.
You have to show the basic informations such as Title and body of the post.

To setup the network interface you're going to implement a very common Android network library: **Retrofit**; also the data is grabbed from a fake online repository (the link to inspect the data structure is below).

The app already contains a folder called *repository* with two files:

1. **JsonPlaceHolderApi**, which represents the interface implementing the GET method to grab the data from the repository.
2. **NetworkRepository**, which represents the class containing the Retrofit implementation.
It's up to you choosing the right architecture design pattern and Retrofit client implementation.

*Useful links:*

1. Retrofit library: https://square.github.io/retrofit/
2. JsonPlaceHolder repository: http://jsonplaceholder.typicode.com/
